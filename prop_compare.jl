using CSV
using DataFrames
using Plots
theme(:ggplot2)

air = CSV.read("AirProp.csv", DataFrame)

f10 = CSV.read("AeroTech_F10.csv", DataFrame)
f15 = CSV.read("Estes_F15.csv", DataFrame)
g8 = CSV.read("AeroTech_G8ST.csv", DataFrame)

plot(
    air.Time,
    air.Thrust,
    label = "Air Propulsion",
    ribbon = (zeros(length(df.Time)), min.(15, air.Uncertainty)),
    fillalpha = 0.1,
    legend = :topleft,
)

for (d, l) in [(f10, "F10"), (f15, "F15"), (g8, "G8ST")]
    plot!(d[!, "Time (s)"], d[!, "Thrust (N)"], label = l)
end

title!("Propulsion Comparison")
xlabel!("Time (s)")
ylabel!("Thrust (N)")

savefig("prop_compare.png")