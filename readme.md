# Air Prop Thrust 

`readme auto generated, run air_prop_sim.jl to recreate.`

![Thrust Over Time](ThrustCurve.png)

Total Impulse: 405.0 ± 40.0 N s

Burn TIme: 20.30300000000183 s

More Data:

<table class="data-frame"><thead><tr><th></th><th>variable</th><th>mean</th><th>min</th><th>median</th><th>max</th><th>nmissing</th><th>eltype</th></tr><tr><th></th><th>Symbol</th><th>Quantity</th><th>Quantity</th><th>Quantity</th><th>Quantity</th><th>Int64</th><th>DataType</th></tr></thead><tbody><p>4 rows × 7 columns</p><tr><th>1</th><td>Thrust</td><td>20.0±2.0 N</td><td>0.0±0.0 N</td><td>21.1±4.7 N</td><td>21.1±4.7 N</td><td>0</td><td>Quantity{Measurement{Float64}, 𝐋 𝐌 𝐓^-2, FreeUnits{(N,), 𝐋 𝐌 𝐓^-2, nothing}}</td></tr><tr><th>2</th><td>Pressure</td><td>1.39e7±3.6e6 Pa</td><td>310000.0±1.1e6 Pa</td><td>1.38e7±3.9e6 Pa</td><td>2.9e7±2.1e6 Pa</td><td>0</td><td>Quantity{Measurement{Float64}, 𝐌 𝐋^-1 𝐓^-2, FreeUnits{(Pa,), 𝐌 𝐋^-1 𝐓^-2, nothing}}</td></tr><tr><th>3</th><td>Time</td><td>10.1515 s</td><td>0.0 s</td><td>10.1515 s</td><td>20.303 s</td><td>0</td><td>Quantity{Float64, 𝐓, FreeUnits{(s,), 𝐓, nothing}}</td></tr><tr><th>4</th><td>Mass</td><td>0.225±0.066 kg</td><td>0.005±0.018 kg</td><td>0.224±0.071 kg</td><td>0.468±0.053 kg</td><td>0</td><td>Quantity{Measurement{Float64}, 𝐌, FreeUnits{(kg,), 𝐌, nothing}}</td></tr></tbody></table>

## Comparison to Rocket Motors

![Propulsion Comparison](prop_compare.png)

https://www.thrustcurve.org/

